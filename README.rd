= Apache Arrow 1.0 - A cross-language development platform for in-memory data

Apache Arrow is a cross-language development platform for in-memory
data. You can use Apache Arrow to process large data effectively in
Python and other languages such as R. Apache Arrow is the future of
data processing. Apache Arrow 1.0, the first major version, was
released at 2020-07-24. It's a good time to know Apache Arrow and
start using it.

== License

=== Slide

CC BY-SA 4.0

Use the followings for notation of the author:

  * Sutou Kouhei

==== ClearCode Inc. logo

CC BY-SA 4.0

Author: ClearCode Inc.

It is used in page header and some pages in the slide.

==== Apache Arrow and Apache Parquet logos

Apache License 2.0

Author: The Apache Software Foundation

==== File icon

CC BY 4.0

Author: Muhammad Yafinuha

==== Memory icon

CC BY 4.0

Author: Philip Sheffield

== For author

=== Show

  rake

=== Publish

  rake publish

== For viewers

=== Install

  gem install rabbit-slide-kou-scipy-japan-2020

=== Show

  rabbit rabbit-slide-kou-scipy-japan-2020.gem

